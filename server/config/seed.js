/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var User = require('../api/user/user.model');

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'password123'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@washable.com.au',
    password: 'password123'
  }, {
    provider: 'local',
    role: 'user',
    name: 'Allyson Taylor',
    email: 'user@washable.com.au',
    password: 'password123'
  },{
    provider: 'local',
    role: 'washer',
    name: 'Frank Lorimbar',
    email: 'washer@washable.com.au',
    password: 'password123'
  }, function() {
      console.log('finished populating users');
    }
  );
});
