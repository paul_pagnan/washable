/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /bookings              ->  index
 * POST    /bookings              ->  create
 * GET     /bookings/:id          ->  show
 * PUT     /bookings/:id          ->  update
 * DELETE  /bookings/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Booking = require('./booking.model');
var Vehicle = require('../vehicle/vehicle.model');
var ObjectId = require('mongoose').Types.ObjectId;

// Get list of bookings
exports.index = function(req, res) {
  Booking.find({washer: {$exists: false}})
  .deepPopulate(
    'user ' +
    'vehicle ' +
    'washer')
  .exec(function (err, bookings) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(bookings);
  });
};

exports.myBookings = function(req, res) {
  Booking.find({ user: new ObjectId(req.user._id) })
  .deepPopulate(
    'user ' +
    'vehicle ' +
    'washer')
  .exec(function (err, bookings) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(bookings);
  });
};

exports.assignedBookings = function(req, res) {
  Booking.find({ washer: new ObjectId(req.user._id) })
  .deepPopulate(
    'user ' +
    'vehicle ' +
    'washer')
  .exec(function (err, bookings) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(bookings);
  });
};

// Get a single booking
exports.show = function(req, res) {
  Booking.findById(req.params.id)
  .deepPopulate(
    'user ' +
    'vehicle ' +
    'washer')
    .exec(function (err, booking) {
      if(err) { return handleError(res, err); }
      if(!booking) { return res.status(404).send('Not Found'); }
      return res.json(booking);
    });
};

// Creates a new booking in the DB.
exports.create = function(req, res) {
  var vehicle = req.body.vehicle;
  if(vehicle !== undefined) {
    Vehicle.create(vehicle, function(err, vehicle) {
      if(err) { return handleError(res, err); }
      req.body.vehicle = vehicle.id;
      req.body.user = req.user._id;
      Booking.create(req.body, function(err, booking) {
        return res.status(201).json(booking);
      });
    });
  } else {
    res.status(400).send('No Vehicle');
  }
};

// Add Washer to Booking in the DB.
exports.accept = function(req,res) {
  Booking.findById(req.params.id, function(err, booking) {
    if (err) { return handleError(res, err); }
    if(!booking) { return res.status(404).send('Booking not found'); }
    booking.washer = req.user._id;
    Booking.update({_id: req.params.id}, booking, {upsert: true}, function(err){
      return res.status(200).json('Success');
    });
  });
}


// Updates an existing booking in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Booking.findById(req.params.id, function (err, booking) {
    if (err) { return handleError(res, err); }
    if(!booking) { return res.status(404).send('Not Found'); }
    var updated = _.merge(booking, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(booking);
    });
  });
};

// Deletes a booking from the DB.
exports.destroy = function(req, res) {
  Booking.findById(req.params.id, function (err, booking) {
    if(err) { return handleError(res, err); }
    if(!booking) { return res.status(404).send('Not Found'); }
    booking.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
