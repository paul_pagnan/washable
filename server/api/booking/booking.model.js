var User = require('../user/user.model');
var Vehicle = require('../vehicle/vehicle.model');

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var BookingSchema = new Schema({
  user: { type: Schema.ObjectId, ref: 'User' },
  vehicle: { type: Schema.ObjectId, ref: 'Vehicle' },

  //Time and Date
  date: Date,
  time: Date,

  // Wash Details
  type: String,
  extra: String,

  //Address
  address1: String,
  address2: String,
  city: String,
  state: String,
  postcode: String,

  //Alternatively, use GPS location
  lat: String,
  long: String,

  // Washer info
  notes: String,
  washer: {type: Schema.ObjectId, ref: 'Washer' },

  // Meta
  created_at: {
    type: Date,
    default: Date.now
  },

  active: Boolean
});
BookingSchema.plugin(deepPopulate);
module.exports = mongoose.model('Booking', BookingSchema);
