'use strict';

var express = require('express');
var controller = require('./booking.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.hasRole('washer'), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.get('/client/mine', auth.isAuthenticated(), controller.myBookings);
router.get('/washer/mine', auth.hasRole('washer'), controller.assignedBookings);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.put('/:id/accept', auth.hasRole('washer'), controller.accept);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;
