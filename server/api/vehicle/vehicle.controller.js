/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /vehicles              ->  index
 * POST    /vehicles              ->  create
 * GET     /vehicles/:id          ->  show
 * PUT     /vehicles/:id          ->  update
 * DELETE  /vehicles/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Vehicle = require('./vehicle.model');

// Get list of vehicles
exports.index = function(req, res) {
  Vehicle.find(function (err, vehicles) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(vehicles);
  });
};

// Get a single vehicle
exports.show = function(req, res) {
  Vehicle.findById(req.params.id, function (err, vehicle) {
    if(err) { return handleError(res, err); }
    if(!vehicle) { return res.status(404).send('Not Found'); }
    return res.json(vehicle);
  });
};

// Creates a new vehicle in the DB.
exports.create = function(req, res) {
  Vehicle.create(req.body, function(err, vehicle) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(vehicle);
  });
};

// Updates an existing vehicle in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Vehicle.findById(req.params.id, function (err, vehicle) {
    if (err) { return handleError(res, err); }
    if(!vehicle) { return res.status(404).send('Not Found'); }
    var updated = _.merge(vehicle, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(vehicle);
    });
  });
};

// Deletes a vehicle from the DB.
exports.destroy = function(req, res) {
  Vehicle.findById(req.params.id, function (err, vehicle) {
    if(err) { return handleError(res, err); }
    if(!vehicle) { return res.status(404).send('Not Found'); }
    vehicle.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
