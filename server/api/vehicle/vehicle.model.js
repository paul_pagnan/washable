const User = require('mongoose').model('User');

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var VehicleSchema = new Schema({
  user: { type: Schema.ObjectId, ref: 'User' },

  //Vehicle Information
  make: String,
  model: String,
  license_plate: String,
  category: String,

  car_accesibility: String,

  // Meta
  created_at: {
    type: Date,
    default: Date.now
  },

  active: Boolean
});

module.exports = mongoose.model('Vehicle', VehicleSchema);
