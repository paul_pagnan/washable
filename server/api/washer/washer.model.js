const User = require('mongoose').model('User');
var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var WasherSchema = new Schema({
  user: { type: Schema.ObjectId, ref: 'User' },

  //Banking Details
  BSB: String,
  acc_number: String,
  acc_name: String,

  // Meta
  created_at: {
    type: Date,
    default: Date.now
  },

  active: Boolean
});

module.exports = mongoose.model('Washer', WasherSchema);
