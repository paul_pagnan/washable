/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var washer = require('./washer.model');

exports.register = function(socket) {
  washer.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  washer.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('washer:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('washer:remove', doc);
}
