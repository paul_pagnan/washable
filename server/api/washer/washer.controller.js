/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /washers              ->  index
 * POST    /washers              ->  create
 * GET     /washers/:id          ->  show
 * PUT     /washers/:id          ->  update
 * DELETE  /washers/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Washer = require('./washer.model');

// Get list of washers
exports.index = function(req, res) {
  Washer.find(function (err, washers) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(washers);
  });
};

// Get a single washer
exports.show = function(req, res) {
  Washer.findById(req.params.id, function (err, washer) {
    if(err) { return handleError(res, err); }
    if(!washer) { return res.status(404).send('Not Found'); }
    return res.json(washer);
  });
};

// Creates a new washer in the DB.
exports.create = function(req, res) {
  Washer.create(req.body, function(err, washer) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(washer);
  });
};

// Updates an existing washer in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Washer.findById(req.params.id, function (err, washer) {
    if (err) { return handleError(res, err); }
    if(!washer) { return res.status(404).send('Not Found'); }
    var updated = _.merge(washer, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(washer);
    });
  });
};

// Deletes a washer from the DB.
exports.destroy = function(req, res) {
  Washer.findById(req.params.id, function (err, washer) {
    if(err) { return handleError(res, err); }
    if(!washer) { return res.status(404).send('Not Found'); }
    washer.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
