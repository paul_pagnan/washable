'use strict';

var mongoose = require('mongoose'), Schema = mongoose.Schema;
var deepPopulate = require('mongoose-deep-populate')(mongoose);

var ApplicationSchema = new Schema({
  //Contact Info
  name:String,
  email: String,
  contact_number: String,

  // Free text message
  message: String,

  // Meta
  date_submitted: {
    type: Date,
    default: Date.now
  },
  confirmed: {
    type: Boolean
  }
});
ApplicationSchema.plugin(deepPopulate);
module.exports = mongoose.model('Application', ApplicationSchema);
