'use strict';

var _ = require('lodash');
var Application = require('./application.model');

// Get list of applications
exports.index = function(req, res) {
  Application.find({$or:[{confirmed:false},{confirmed:{$exists:false}}]}, function (err, applications) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(applications);
  });
};

exports.acceptedApplications = function(req, res) {
  Application.find({confirmed:true}, function (err, apps) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(apps);
  });
};

// Get a single application
exports.show = function(req, res) {
  Application.findById(req.params.id, function (err, application) {
    if(err) { return handleError(res, err); }
    if(!application) { return res.status(404).send('Not Found'); }
    return res.json(application);
  });
};

// Creates a new application in the DB.
exports.create = function(req, res) {
  Application.create(req.body, function(err, application) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(application);
  });
};

// Updates an existing application in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Application.findById(req.params.id, function (err, application) {
    if (err) { return handleError(res, err); }
    if(!application) { return res.status(404).send('Not Found'); }
    var updated = _.merge(application, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(application);
    });
  });
};

// Deletes a application from the DB.
exports.destroy = function(req, res) {
  Application.findById(req.params.id, function (err, application) {
    if(err) { return handleError(res, err); }
    if(!application) { return res.status(404).send('Not Found'); }
    application.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
