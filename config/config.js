'use strict';

angular.module('washableApp')
  .constant('washableConfig', {
    api: '@@api',
    web_uri: '@@web_uri'
  });
