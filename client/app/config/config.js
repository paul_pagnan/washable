'use strict';

angular.module('washableApp')
  .constant('washableConfig', {
    api: 'http://washable.herokuapp.com/api/',
    web_uri: 'http://washable.herokuapp.com/'
  });
