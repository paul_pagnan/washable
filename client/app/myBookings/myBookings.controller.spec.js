'use strict';

describe('Controller: MyBookingsCtrl', function () {

  // load the controller's module
  beforeEach(module('washableApp'));

  var MyBookingsCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MyBookingsCtrl = $controller('MyBookingsCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
