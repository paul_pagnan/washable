'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/myBookings', {
        templateUrl: 'app/myBookings/myBookings.html',
        controller: 'MyBookingsCtrl',
        authenticate: true
      });
  });
