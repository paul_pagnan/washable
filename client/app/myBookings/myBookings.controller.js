'use strict';

angular.module('washableApp')
  .controller('MyBookingsCtrl', function ($scope, toastr, Booking, $location) {
    $scope.title = 'My Bookings';
    $scope.bookings = {};
    $scope.loaded = false;

    //Invoke the API
    Booking.myBookings(function(bookings) {
      //Set the response to the scope
      $scope.loaded = true;
      $scope.bookings = bookings;
    }, function(err) {
      console.log(err);
      toastr.error(err.data, "Failed to get bookings");
    });

    $scope.nav = function(obj) {
      $location.path("/booking/" + obj._id);
    }
  });
