'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
      $routeProvider
        .when('/', {
          templateUrl: 'app/main/main.html',
          controller: 'MainCtrl',
          authenticate: true
        });
  });
