'use strict';

angular.module('washableApp')
  .controller('LoginCtrl', function ($scope, Auth, $location, User, $window) {
    $scope.user = {};
    $scope.errors = {};

    $scope.login = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Logged in, redirect to home
          User.get(function(user) {
            switch (user.role) {
              case "washer":
                $location.path('/washer');
                break;
              case "admin":
                $location.path('/applications');
                break;
              default:
                $location.path('/');
            }
          });

        })
        .catch( function(err) {
          $scope.errors.other = err.message;
        });
      }
    };

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
