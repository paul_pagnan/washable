'use strict';

angular.module('washableApp')
  .controller('SignupCtrl', function ($scope, Auth, $location, $window, Application, toastr) {
    $scope.user = {};
    $scope.errors = {};
    $scope.washer = false;

    $scope.apply = function(form) {
      Application.create($scope.application, function() {
          toastr.success("Application submitted successfully");
        }, function(err) {
          toastr.error(err.data, "Failed to create booking");
        });
    };

    $scope.register = function(form) {

      $scope.submitted = true;

      if(form.$valid) {
        if ($scope.washer) {
          $scope.apply();
        }
        Auth.createUser({
          name: $scope.user.name,
          email: $scope.user.email,
          contact_number: $scope.user.contact_number,
          password: $scope.user.password
        })

        .then( function() {
          // Account created, redirect to home
          if (!$scope.washer) {
            $location.path('/');
          } else {
            Auth.logout();
            $location.path('/login');
          }
        })
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            console.error(error);
            console.error(field);
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };

    $scope.loginOauth = function(provider) {
      $window.location.href = '/auth/' + provider;
    };
  });
