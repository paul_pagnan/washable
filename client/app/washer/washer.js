'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
      $routeProvider
        .when('/washer', {
          templateUrl: 'app/washer/washer.html',
          controller: 'WasherCtrl',
          authenticate: true
        });
  });
