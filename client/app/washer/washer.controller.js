'use strict';

angular.module('washableApp')
  .controller('WasherCtrl', function ($scope, $http, socket, Booking, Auth, toastr, $location) {
      $scope.title='Washable';

      $scope.assigned = {};
      $scope.bookings = {};
      $scope.loaded = false;
      $scope.loadedAssigned = false;

      $scope.booking;

      $scope.currentUser = Auth.currentUser();


      //Invoke the API
      Booking.index(function(bookings) {
        //Set the response to the scope
        $scope.bookings = bookings;
        $scope.loaded = true;
      }, function(err) {
        toastr.error(err.data, "Failed to get bookings");
      });

      Booking.assigned(function(assignedBookings) {
        //Set the response to the scope
        $scope.assigned = assignedBookings;
        $scope.loadedAssigned = true;
      }, function(err) {
        toastr.error(err.data, "No bookings are assigned");
      });

      $scope.nav = function(obj) {
        $location.path("/booking/" + obj._id);
      }
  });
