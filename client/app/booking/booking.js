'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/booking/:id', {
        templateUrl: 'app/booking/booking.html',
        controller: 'BookingCtrl'
      });
  });
