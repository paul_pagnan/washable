'use strict';

angular.module('washableApp')
  .controller('BookingCtrl', function ($scope, $http, socket, Booking, toastr, $route, User, $mdDialog, $location) {
    $scope.title = 'Booking';

    $scope.booking;
    $scope.booking = {};
    $scope.isWasher = {};
    User.get(function(user) {
      $scope.isWasher = (user.role == "washer");
    });
    Booking.get({id: $route.current.params.id}, function(booking) {
      //Set the response to the scope
      $scope.booking = booking;
    }, function(err) {
      toastr.error(err.data, "No booking");
    });

    $scope.cancelDialog = function(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
         var confirm = $mdDialog.confirm()
               .title('Are you sure?')
               .content('Are you sure you want to cancel your wash booking?')
               .targetEvent(ev)
               .ok('Yes')
               .cancel('No');
         $mdDialog.show(confirm).then(function() {
            Booking.delete({id: $route.current.params.id}, function() {
              $location.path('/');
              toastr.success("Booking cancelled successfully", "Success");
            }, function(err) {
              toastr.error(err.data, "Error");
            });
         });
    }

    $scope.acceptDialog = function(ev) {
      // Appending dialog to document.body to cover sidenav in docs app
         var confirm = $mdDialog.confirm()
               .title('Are you sure?')
               .content('Are you sure you want to accept this wash booking?')
               .targetEvent(ev)
               .ok('Yes')
               .cancel('No');
         $mdDialog.show(confirm).then(function() {
            Booking.accept({id: $route.current.params.id}, {}, function() {
              $location.path('/washer');
              toastr.success("Booking accepted successfully", "Success");
            }, function(err) {
              toastr.error(err.data, "Error");
            });
         });
    }

    $scope.carSizes = [{
        title: "Small",
        svg: 'assets/images/hatch.svg',
        value: 'Small',
        price: -5
      },{
        title: "Medium",
        svg: 'assets/images/sedan.svg',
        value: 'Medium',
        price: 0
      },{
        title: "Large",
        svg: 'assets/images/suv.svg',
        value: 'Large',
        price: 5
    }];

    $scope.washTypes = [{
        title: "Exterior",
        svg: 'assets/images/wash1.svg',
        value: 'Exterior',
        price: 10
      },{
        title: "Inside and Out",
        svg: 'assets/images/wash2.svg',
        value: 'Inside and Out',
        price: 15
      },{
        title: "Showroom Spec",
        svg: 'assets/images/wash3.svg',
        value: 'Showroom Spec',
        price: 20
    }];

    $scope.extras = [{
        title: "Air Freshener",
        svg: 'assets/images/air.svg',
        value: 'Air Freshener',
        price: 5
      },{
        title: "Tyre Shine",
        svg: 'assets/images/tyre.svg',
        value: 'Tyre Shine',
        price: 5
    }];

    $scope.carDetail = function() {
      var result = $.grep($scope.carSizes, function(e){ return e.value == $scope.booking.vehicle.category; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.washDetail = function() {
      var result = $.grep($scope.washTypes, function(e){ return e.value == $scope.booking.type; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.extraDetail = function() {
      var result = $.grep($scope.extras, function(e){ return e.value == $scope.booking.extra; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.total = function() {
      var extra = ($scope.extraDetail().price === undefined) ? 0 : $scope.extraDetail().price;
      return $scope.carDetail().price + $scope.washDetail().price + extra;
    }
  });
