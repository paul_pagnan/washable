'use strict';

angular.module('washableApp')
  .provider('Booking', function () {
    this.$get = ['$resource', 'washableConfig', function($resource, washableConfig) {
      var Booking = $resource(washableConfig.api + 'bookings/:id', {},
      {
        update: { method: 'PUT' },
        create: { method: 'POST' },
        index:   { method: 'GET', isArray: true },
        myBookings:   {
          method: 'GET',
          url: washableConfig.api + 'bookings/client/mine',
          isArray: true
        },
        assigned:   {
          method: 'GET',
          url: washableConfig.api + 'bookings/washer/mine',
          isArray: true
        },
        accept:   {
          method: 'PUT',
          url: washableConfig.api + 'bookings/:id/accept'
        }
      });
      return Booking;
    }];
  });
