'use strict';

angular.module('washableApp')
  .provider('Application', function () {
    this.$get = ['$resource', 'washableConfig', function($resource, washableConfig) {
      var Application = $resource(washableConfig.api + 'applications/:id', {},
      {
        acceptedApps:   {
          method: 'GET',
          url: washableConfig.api + 'applications/washer/accepted',
          isArray: true
        },
        update: { method: 'PUT' },
        create: { method: 'POST' },
        index:   { method: 'GET', isArray: true }

      });
      return Application;
    }];
  });
