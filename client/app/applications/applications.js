'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/applications', {
        templateUrl: 'app/applications/applications.html',
        controller: 'ApplicationsCtrl'
      });
  });
