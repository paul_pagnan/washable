'use strict';

angular.module('washableApp')
  .controller('ApplicationsCtrl', function ($scope, Application, toastr) {
    $scope.title = 'Applications';
    $scope.applications = {};
    $scope.accepted = {};
    $scope.loaded = false;
    $scope.loadedAccepted = false;

    Application.index(function(applications) {
      //Set the response to the scope
      $scope.applications = applications;
      $scope.loaded = true;
      console.log(applications);
    }, function(err) {
      toastr.error(err.data, "Failed to get applications");
    });

    Application.acceptedApps(function(accepted) {
      //Set the response to the scope
      $scope.accepted = accepted;
      $scope.loadedAccepted = true;
    }, function(err) {
      console.log(err);
      toastr.error(err.data, "Failed to get accepted applications");
    });

    $scope.accept = function(application) {
      application.confirmed = true;
     Application.update({id: application._id }, application, function(accepted) {
       $scope.accepted.unshift(application);
       $scope.applications = $scope.applications
                .filter(function (x) {
                         return x._id !== application._id;
                        });
      toastr.success("Application accepted successfully", "Success");
     }, function(err) {
       console.log(err);
       toastr.error(err.data, "Failed to accept application");
     });
    };

    $scope.deny = function(application) {
      Application.delete({id: application._id}, function() {
        toastr.success("Application denied successfully", "Success");
      }, function(err) {
        toastr.error(err.data, "Failed to deny application");
      });
    };
  });
