'use strict';

angular.module('washableApp')
  .controller('BookCtrl', function ($scope, $location, geolocation, Booking, toastr) {
    $scope.title = 'Book a Wash';
    $scope.current = 1;
    $scope.booking = {};
    $scope.vehicle = {};
    $scope.data = {};
    $scope.forms = {};
    $scope.formValid = true;

    $scope.carSizes = [{
        title: "Small",
        svg: 'assets/images/hatch.svg',
        value: 'Small',
        price: -5
      },{
        title: "Medium",
        svg: 'assets/images/sedan.svg',
        value: 'Medium',
        price: 0
      },{
        title: "Large",
        svg: 'assets/images/suv.svg',
        value: 'Large',
        price: 5
    }];

    $scope.washTypes = [{
        title: "Exterior",
        svg: 'assets/images/wash1.svg',
        value: 'Exterior',
        price: 10
      },{
        title: "Inside and Out",
        svg: 'assets/images/wash2.svg',
        value: 'Inside and Out',
        price: 15
      },{
        title: "Showroom Spec",
        svg: 'assets/images/wash3.svg',
        value: 'Showroom Spec',
        price: 20
    }];

  // Removed extras to simplify
    $scope.extras = [{
        title: "Air Freshener",
        svg: 'assets/images/air.svg',
        value: 'Air Freshener',
        price: 5
      },{
        title: "Tyre Shine",
        svg: 'assets/images/tyre.svg',
        value: 'Tyre Shine',
        price: 5
    }];
    $scope.states = ('NSW QLD WA ACT TAS VIC SA NT').split(' ').map(function (state) { return { abbrev: state }; });

    $scope.next = function() {
      if($scope.forms['f' + $scope.current].$valid) {
          $scope.current++;
          $scope.formValid = true;
      } else {
        $scope.formValid = false;
      }
    };
    $scope.back = function() {
      $scope.current--;
    };

    $scope.book = function() {
      console.log($scope.booking);
      Booking.create($scope.booking, function() {
          toastr.success("Booking created successfully");
          $location.path('/');
        }, function(err) {
          toastr.error(err.data, "Failed to create booking");
        });
    };

    $scope.$on('mapInitialized', function(event, map) {
      geolocation.getLocation().then(function(data){
        map.setCenter(data.coords.latitude, data.coords.longitude);
      });
    });

    $scope.carDetail = function() {
      var result = $.grep($scope.carSizes, function(e){ return e.value == $scope.booking.vehicle.category; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.washDetail = function() {
      var result = $.grep($scope.washTypes, function(e){ return e.value == $scope.booking.type; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.extraDetail = function() {
      var result = $.grep($scope.extras, function(e){ return e.value == $scope.booking.extra; });
      return (result.length > 0) ? result[0] : "";
    };

    $scope.total = function() {
      var extra = ($scope.extraDetail().price === undefined) ? 0 : $scope.extraDetail().price;
      return $scope.carDetail().price + $scope.washDetail().price + extra;
    }

    //ngAutocomplete
     $scope.result1 = 'initial value';
     $scope.options1 = null;
     $scope.details1 = '';
  });
