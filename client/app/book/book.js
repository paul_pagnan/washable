'use strict';

angular.module('washableApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/book', {
        templateUrl: 'app/book/book.html',
        controller: 'BookCtrl',
        authenticate: true
      });
  });
