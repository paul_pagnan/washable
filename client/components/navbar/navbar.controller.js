'use strict';

angular.module('washableApp')
  .controller('NavbarCtrl', function ($scope, $location, $mdDialog, Auth) {

    $scope.menuItems = [{ title: "Logout" }];
    var homePaths = ['/', '/washer', '/applications']
    $scope.showBack = homePaths.indexOf($location.path()) == -1;

    var originatorEv;
    this.openMenu = function($mdOpenMenu, ev) {
      originatorEv = ev;
      $mdOpenMenu(ev);
    };

    $scope.logout = function() {
      Auth.logout();
      $location.path('/login');
    };

    $scope.back = function() {
      window.history.back();
    };
  });
