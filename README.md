# Washable #
Washable is an online platform where users can book to have their car washed on demand, anytime, anywhere. Registered users can access the service through a PC or mobile device. Using an intuitive interface, customers are able to be connected with available washers to schedule a time and place to have the customer’s car washed. Payment should be handled through the website so that cash does not have to be handled in person


# Install Instructions
1. Download and install node.js for your OS from [https://nodejs.org/en/download/](https://nodejs.org/en/download/)
2. Download and install bower by typing `npm install -g bower` into a node.js enabled terminal. Full instructions at: [http://bower.io/](http://bower.io/)
3. Download and install MongoDb from [https://docs.mongodb.org/manual/installation/](https://docs.mongodb.org/manual/installation/)
3. Open a new terminal window and change directory into the main washable directory
4. Run `npm install` to download and install the project's node dependencies
5. Run `bower install` to download and install the project's bower dependencies
6. Once completed successfully, run the `grunt serve` command to launch the web server and start the project
7. The project will open in your default web browser automatically, but you can access it manually by navigating to [http://localhost:9000](http://localhost:9000)

NOTE: The above instructions have been tested for node.js version v0.12.7

# Screenshots
### User Dashboard
![washable.png](https://bitbucket.org/repo/KqEoGK/images/2659224483-washable.png)

### Book A Wash
![book.png](https://bitbucket.org/repo/KqEoGK/images/2108668620-book.png)

### My Bookings
![Bookings.png](https://bitbucket.org/repo/KqEoGK/images/2626586201-Bookings.png)